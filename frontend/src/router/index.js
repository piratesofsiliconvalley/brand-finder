import Vue from 'vue'
import Router from 'vue-router'
import Map from '../components/Map'
import Header from '../components/Header'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Map',
      component: Map
    },
    {
      path: '/',
      name: 'Header',
      component: Header
    }
  ]
})
